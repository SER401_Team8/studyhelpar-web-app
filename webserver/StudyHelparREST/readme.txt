Generates RESTful API to interact with the database in the following ways:
-getTextbooks returns a list of all entries in the textbook table
-getTextbook returns a specific entry in the textbook table given its id
-createTextbook creates an entry in the textbook table
-deleteTextbook delete a specific entry in the textbook table given its id
-updateTextbook updates an entry in the textbook table given its id

Tools Needed:
-Local Tomcat server needs to be set up (reference points need to be updated accordingly in build.properties)
-MySQL server needs to be set up
	1) Create a database that has a table named textbook
	2) The textbook table needs to have the following fields: id, name, code, image, arcontentdescription

Configuration:
-Use rdbm.properties to update the MySQL server settings.
-Use build.properties to update the Tomcat settings.
-Add an images folder to the Tomcat library.