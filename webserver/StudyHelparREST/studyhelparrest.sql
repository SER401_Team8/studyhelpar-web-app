SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: DATABASE studyhelpar; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON DATABASE studyhelparrest IS 'The Study Helapr Database for SER402 Study Helpar.';

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'Standard public schema';
SET search_path = public, pg_catalog;

--
-- Name: textbook; Type: TABLE; Schema: public; Owner: Anup Chagam; Tablespace:
--

CREATE TABLE textbook (
    id integer NOT NULL,
    name text NOT NULL,
    code text,
    image text,
    arcontentdescription text
);

--
-- Data for Name: textbook; Type: TABLE DATA; Schema: public; Owner: Anup Chagam
--

INSERT INTO textbook VALUES (86049, 'Testing Database', 'Database1111', 'image-database', 'arcontent-database');
INSERT INTO textbook VALUES (48008, 'Python for dummies', 'Code2222', 'image-python', 'arcontent-python');

--
-- Name: textbook_id_pkey; Type: CONSTRAINT; Schema: public; Owner: Anup Chagam; Tablespace:
--

ALTER TABLE ONLY textbook
    ADD CONSTRAINT textbook_id_pkey PRIMARY KEY (id);