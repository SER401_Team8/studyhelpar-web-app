package edu.asupoly.ser402.studyhelpar.api;

import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
//import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
//import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
//import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

//import org.apache.commons.io.IOUtils;
//import org.jboss.resteasy.plugins.providers.multipart.InputPart;
//import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
//import org.jboss.resteasy.plugins.providers.multipart.*;

import com.fasterxml.jackson.databind.ObjectMapper;
/*
import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.MimeBodyPart;
import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.MimeMultipart;
import com.sun.xml.internal.ws.encoding.MimeMultipartParser;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
*/

import org.glassfish.jersey.media.multipart.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
//import java.sql.SQLException;
import java.nio.file.Paths;

import edu.asupoly.ser402.studyhelpar.model.Textbook;
import edu.asupoly.ser402.studyhelpar.services.StudyHelparService;
import edu.asupoly.ser402.studyhelpar.services.StudyHelparServiceFactory;

//text book name, text book code, text book image, AR content name
@Path("/textbooks")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class TextbookResource {
	private static StudyHelparService __bService = StudyHelparServiceFactory.getInstance();
	
	// Technique for location header taken from
	// http://usna86-techbits.blogspot.com/2013/02/how-to-return-location-header-from.html
	@Context
	private UriInfo _uriInfo;
	
	 /**
     * @apiDefine BadRequestError
     * @apiError (Error 4xx) {400} BadRequest Bad Request Encountered
     * */
    /** @apiDefine ActivityNotFoundError
     * @apiError (Error 4xx) {404} NotFound Activity cannot be found
     * */
    /**
     * @apiDefine InternalServerError
     * @apiError (Error 5xx) {500} InternalServerError Something went wrong at server, Please contact the administrator!
     * */
    /**
     * @apiDefine NotImplementedError
     * @apiError (Error 5xx) {501} NotImplemented The resource has not been implemented. Please keep patience, our developers are working hard on it!!
     * */

    /**
     * @api {get} /textbooks Get list of Text books
     * @apiName getTextbooks
     * @apiGroup Textbooks
     *
     * @apiUse BadRequestError
     * @apiUse InternalServerError
     * 
     * @apiSuccessExample Success-Response:
     * 	HTTP/1.1 200 OK
     * 	[
     *   {"textbookId":1111,"firstName":"Ariel","lastName":"Denham"},
     *   {"textbookId":1212,"firstName":"John","lastName":"Worsley"}
     *  ]
     * 
     * */
	@GET
	public List<Textbook> getTextbooks() {
		return __bService.getTextbooks();
	}

	/* 
	 * This is a second version - it uses Jackson's default mapping via ObjectMapper, which spits out
	 * the same JSON as Jersey's internal version, so the output will look the same as version 1 when you run
	 */
	@GET
	@Path("/{textbookId}")
	public Response getTextbook(@PathParam("textbookId") int aid) {
		// This isn't correct - what if the textbookId is not for an active textbook?
		Textbook textbook = __bService.getTextbook(aid);
		
		// let's use Jackson instead. ObjectMapper will build a JSON string and we use
		// the ResponseBuilder to use that. Note the result looks the same
		try {
			String aString = new ObjectMapper().writeValueAsString(textbook);
			return Response.status(Response.Status.OK).entity(aString).build();
		} catch (Exception exc) {
			exc.printStackTrace();
			return null;
		}
	}

	@GET
	@Path("/image/{textbookId}")
	@Produces("image/png")
	@Consumes("image/png")
	//@PathVariable String filePath
	public Response getImage(HttpServletResponse response, @PathParam("textbookId") int aid) throws IOException {
		Textbook textbook = __bService.getTextbook(aid);
		File file = new File(textbook.getTextbookImage().toString());
	    if(file.exists()){
	    	FileInputStream fileStream = new FileInputStream(file);
	    	return Response.ok(fileStream).build();
	    }else {
	        throw new FileNotFoundException();
	    }
	}
	
	/*
	 * This was the second version that added simple custom response headers and payload
	 * Post in the following order:
	 * Name
	 * Code
	 * Image
	 * Content Description
	 */
	@POST
	@Produces("application/json")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createTextbook(@FormDataParam("file") InputStream uploadedInputStream, @FormDataParam("file") FormDataContentDisposition fileDetail, @FormDataParam("name") String name, @FormDataParam("code") String code, @FormDataParam("image") String image, @FormDataParam("arcontentdescription") String arcontentdescription) throws IOException{
		//The path to the folder where we want to store the uploaded files		
		Properties dbProperties = new Properties();
		dbProperties.load(StudyHelparServiceFactory.class.getClassLoader().getResourceAsStream("/studyhelpar.properties"));
		String UPLOAD_FOLDER = dbProperties.getProperty("uploaddir");
		
		int aid = __bService.createTextbook(name, code, image, arcontentdescription);
		
		if (aid == -1) {
			return Response.status(500).entity("{ \" EXCEPTION INSERTING INTO DATABASE! \"}").build();
		} else if (aid == 0) {
			return Response.status(500).entity("{ \" ERROR INSERTING INTO DATABASE! \"}").build();
		}
		
		String fileName = image + Integer.toString(aid) + ".png";
		String uploadedFileLocation = UPLOAD_FOLDER + fileName;
		
		if (uploadedInputStream == null || uploadedFileLocation == null) {
			return Response.status(400).entity("Invalid form data").build();  
		}
		
		try {
			saveToFile(uploadedInputStream, uploadedFileLocation);
		} catch (IOException e) {
			return Response.status(500).entity("Can not save file").build();
		}
		
		return Response.status(201)
				.header("Location", String.format("%s/%s",_uriInfo.getAbsolutePath().toString(), aid))
				.entity("{ \"Textbook\" : \"" + uploadedFileLocation + "\"}").build();
    }
	
	/**
	 * Utility method to save InputStream data to target location/file
	 * 
	 * @param inStream
	 *            - InputStream to be saved
	 * @param target
	 *            - full path to destination file
	 */
	private void saveToFile(InputStream inStream, String target) throws IOException {
		OutputStream out = null;
		int read = 0;
		byte[] bytes = new byte[1024];
		out = new FileOutputStream(new File(target));
		while ((read = inStream.read(bytes)) != -1) {
			out.write(bytes, 0, read);
		}
		
		out.flush();
		out.close();
	}

	/*
	 * This 2nd version of PUT uses the deserializer from TextbookSerializationHelper, and process the JSON given
	 * in GET version 3 above. Note that when you use the custom serializer/deserializer, it will not be 
	 * compatible with methods that do not use it (which will continue to use the Jersey default). If you
	 * decide to customize, then you should be certain to use your (de)serializer throughout your resource!
	 */
	@PUT
	@Produces("application/json")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response updateTextbook(@FormDataParam("file") InputStream uploadedInputStream, @FormDataParam("file") FormDataContentDisposition fileDetail, @FormDataParam("id") String id, @FormDataParam("name") String name, @FormDataParam("code") String code, @FormDataParam("image") String image, @FormDataParam("arcontentdescription") String arcontentdescription) throws IOException{		
		try {
			//The path to the folder where we want to store the uploaded files		
			Properties dbProperties = new Properties();
			dbProperties.load(StudyHelparServiceFactory.class.getClassLoader().getResourceAsStream("/studyhelpar.properties"));
			String UPLOAD_FOLDER = dbProperties.getProperty("uploaddir");
			String uploadedFileLocation = UPLOAD_FOLDER + image + id + ".png";
			
			Textbook input = new Textbook(Integer.parseInt(id),name,image,code,arcontentdescription);
			
			if (__bService.updateTextbook(input)){
				
				if (uploadedInputStream == null || uploadedFileLocation == null) {
					return Response.status(400).entity("Invalid form data").build();  
				}
				
				try {
					saveToFile(uploadedInputStream, uploadedFileLocation);
				} catch (IOException e){
					return Response.status(500).entity("Can not save file").build();
				}
				
				return Response.status(201)
						.header("Location", String.format("%s/%s",_uriInfo.getAbsolutePath().toString(), id))
						.entity("{ \"Textbook\" : \"" + "Textbook successfully updated" + "\"}").build();
			} else {
				//return Response.status(404, "{ \"message \" : \"No such Textbook " + a.getTextbookId() + "\"}").build();
				return Response.status(404, "{ \"message \" : \"No such Textbook " + "\"}").build();
			}
		} catch (Exception exc) {
			exc.printStackTrace();
			return Response.status(500, "{ \"message \" : \"Internal server error deserializing Textbook JSON\"}").build();
		}
    }
	
	@DELETE
	@Path("/{textbookId}")
    public Response deleteTextbook(@PathParam("textbookId") int aid) throws IOException {
		Properties dbProperties = new Properties();
		dbProperties.load(StudyHelparServiceFactory.class.getClassLoader().getResourceAsStream("/studyhelpar.properties"));
		String UPLOAD_FOLDER = dbProperties.getProperty("uploaddir");
		String uploadedFileLocation = UPLOAD_FOLDER + __bService.getTextbook(aid).getTextbookImage() + aid + ".png";
		
		if (__bService.deleteTextbook(aid)) {
			 try
		        { 
		           Files.deleteIfExists(Paths.get(uploadedFileLocation)); 
		        } 
		        catch(NoSuchFileException e) 
		        { 
		            System.out.println("No such file/directory exists"); 
		        } 
		        catch(DirectoryNotEmptyException e) 
		        { 
		            System.out.println("Directory is not empty."); 
		        } 
		        catch(IOException e) 
		        { 
		            System.out.println("Invalid permissions."); 
		        }

			return Response.status(204).build();
		} else {
			return Response.status(404, "{ \"message \" : \"No such Textbook " + aid + "\"}").build();
		}
    }
}