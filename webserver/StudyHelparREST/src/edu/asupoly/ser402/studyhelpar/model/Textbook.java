package edu.asupoly.ser402.studyhelpar.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Textbook {
	private int    __id;
	private String __name;
	private String __code;
	private String __image;
	private String __arcontentdesc;
	
	public Textbook() {}
	
	public Textbook(int id, String name, String code, String image, String arcontentdesc) {
		__id = id;
		__name  = name;
		__code = code;
		__image = image;
		__arcontentdesc = arcontentdesc;
	}
	public int getTextbookId() {
		return __id;
	}
	public String getTextbookName() {
		return __name;
	}
	public String getTextbookCode() {
		return __code;
	}
	public String getTextbookImage() {
		return __image;
	}
	public String getTextbookARContentDesc() {
		return __arcontentdesc;
	}
	public void setTextbookId(int id) {
		this.__id = id;
	}
	public void setTextbookName(String name) {
		this.__name = name;
	}
	public void setTextbookCode(String code) {
		this.__code = code;
	}
	public void setTextbookImage(String image) {
		this.__image = image;
	}
	public void setTextbookARContentDesc(String arcontentdesc) {
		this.__arcontentdesc = arcontentdesc;
	}
	
	public String toString() {
		return "Textbook ID " + getTextbookId() + ", name " + getTextbookName() + ", code " + getTextbookCode() + ", image " + getTextbookImage() + ", arcontent " + getTextbookARContentDesc();
	}
}