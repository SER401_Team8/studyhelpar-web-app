package edu.asupoly.ser402.studyhelpar.services;

import java.util.List;

import edu.asupoly.ser402.studyhelpar.model.Textbook;

// we'll build on this later
public interface StudyHelparService {
	// Textbook methods
    public List<Textbook> getTextbooks();
    public Textbook getTextbook(int id);
    public boolean deleteTextbook(int id);
    public int createTextbook(String name, String code, String image, String arcontentdesc);
    public boolean updateTextbook(Textbook textbook);
    
    /*
    // Book methods
    public List<Book> getBooks();
    public Book getBook(int id);
    public int createBook(String title, int aid, int sid);
    public Textbook findAuthorOfBook(int bookId);
    
    // Subject methods
    public List<Subject> getSubjects();
    public Subject getSubject(int id);
    public List<Book> findBooksBySubject(int subjectId);
    */
}
