package edu.asupoly.ser402.studyhelpar.services;

import java.util.Properties;

import edu.asupoly.ser402.studyhelpar.services.impl.RDBMStudyHelparServiceImpl;
import edu.asupoly.ser402.studyhelpar.services.impl.SimpleStudyHelparServiceImpl;

// we'll build on this later
public class StudyHelparServiceFactory {
    private StudyHelparServiceFactory() {}

    public static StudyHelparService getInstance() {
	// should really read from a property here
	if (__studyhelpar == null) {
	    __studyhelpar = new RDBMStudyHelparServiceImpl();
	}
	return __studyhelpar;
    }

    private static StudyHelparService __studyhelpar;
    
	// This class is going to look for a file named booktown.properties in the classpath
	// to get its initial settings
	static {
		try {
			Properties dbProperties = new Properties();
			Class<?> initClass = null;
			dbProperties.load(StudyHelparServiceFactory.class.getClassLoader().getResourceAsStream("/studyhelpar.properties"));
			String serviceImpl = dbProperties.getProperty("serviceImpl");
			if (serviceImpl != null) {
				initClass = Class.forName(serviceImpl);
			} else {
				initClass = Class.forName("edu.asupoly.ser402.studyhelpar.services.impl.SimpleStudyHelparServiceImpl.java");
			}
			__studyhelpar = (StudyHelparService)initClass.newInstance();
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
		}
	}
    
}
