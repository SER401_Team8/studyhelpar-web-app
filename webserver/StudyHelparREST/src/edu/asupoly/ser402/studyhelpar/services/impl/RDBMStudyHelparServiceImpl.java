package edu.asupoly.ser402.studyhelpar.services.impl;

import edu.asupoly.ser402.studyhelpar.model.Textbook;
import edu.asupoly.ser402.studyhelpar.services.StudyHelparService;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

//A simple impl of interface StudyHelparService
public class RDBMStudyHelparServiceImpl extends AStudyHelparServiceImpl {
	private static Properties __dbProperties;
	private static String __jdbcUrl;
	private static String __jdbcUser;
	private static String __jdbcPasswd;
	private static String __jdbcDriver;
	private static String error;
	
	private Connection getConnection() throws Exception {
		try {
			Class.forName(__jdbcDriver);
			return DriverManager.getConnection(__jdbcUrl, __jdbcUser, __jdbcPasswd);
		} catch (Exception exc) {
			error = exc.getMessage();
			//throw exc;
			return null;
		}
	}

	// Only instantiated by factory within package scope
	public RDBMStudyHelparServiceImpl() {
	}

	public List<Textbook> getTextbooks() {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		List<Textbook> rval = new ArrayList<Textbook>();
		
		try {
			conn = getConnection();

			if(conn == null)
				rval.add(new Textbook(50938, error, __jdbcUrl, __jdbcUser, __jdbcPasswd));
			else {
				stmt = conn.createStatement();
				rs = stmt.executeQuery(__dbProperties.getProperty("sql.getTextbooks"));
				while (rs.next()) {
					rval.add(new Textbook(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
				}
			}
		}
		catch (Exception se) {
			se.printStackTrace();
			return null;
		}
		finally {  // why nest all of these try/finally blocks?
			try {
				if (rs != null) { rs.close(); }
			} catch (Exception e1) { e1.printStackTrace(); }
			finally {
				try {
					if (stmt != null) { stmt.close(); }
				} catch (Exception e2) { e2.printStackTrace(); }
				finally {
					try {
						if (conn != null) { conn.close(); }
					} catch (Exception e3) { e3.printStackTrace(); }
				}
			}
		}

		return rval;
	}

	// This class is going to look for a file named rdbm.properties in the classpath
	// to get its initial settings
	static {
		try {
			__dbProperties = new Properties();
			__dbProperties.load(RDBMStudyHelparServiceImpl.class.getClassLoader().getResourceAsStream("rdbm.properties"));
			__jdbcUrl    = __dbProperties.getProperty("jdbcUrl");
			__jdbcUser   = __dbProperties.getProperty("jdbcUser");
			__jdbcPasswd = __dbProperties.getProperty("jdbcPasswd");
			__jdbcDriver = __dbProperties.getProperty("jdbcDriver");
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
		}
	}

	@Override
	public Textbook getTextbook(int id){
		List<Textbook> rval = getTextbooks();

		for (int i = 0; i < rval.size(); i++) {
			if (rval.get(i).getTextbookId() == id) {
				return rval.get(i);
			}
		}
		return null;
	}

	@Override
	public boolean deleteTextbook(int id) {
		boolean rval = false;
		Connection conn = null;
		PreparedStatement stmt  = null;
		
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(__dbProperties.getProperty("sql.deleteTextbook"));
			stmt.setInt(1, id);
			rval = (stmt.executeUpdate() > 0);
			return rval;
		} catch (Exception sqe) {
			sqe.printStackTrace();
			return false;
		} finally {  // why nest all of these try/finally blocks?
			try {
					if (stmt != null) { stmt.close(); }
			} catch (Exception e2) { e2.printStackTrace(); }
			finally {
				try {
					if (conn != null) { conn.close(); }
				} catch (Exception e3) { e3.printStackTrace(); }
			}
		}
	}

	@Override
	public int createTextbook(String name, String code, String image, String arcontentdesc) {
		if (name == null || code == null || image == null || arcontentdesc == null) {
			return -1;
		}
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(__dbProperties.getProperty("sql.createTextbook"));
			int generatedKey = generateKey(1, 99999);
			stmt.setInt(1, generatedKey); 
			stmt.setString(2, name);
			stmt.setString(3, code);
			stmt.setString(4, image);
			stmt.setString(5, arcontentdesc);
			int updatedRows = stmt.executeUpdate();
			if(updatedRows > 0){
				return generatedKey;
			}else{
				return -1;
			}
		} catch (Exception sqe) {
			sqe.printStackTrace();
			return -1;
		} finally {  // why nest all of these try/finally blocks?
			try {
					if (stmt != null) { stmt.close(); }
			} catch (Exception e2) { e2.printStackTrace(); }
			finally {
				try {
					if (conn != null) { conn.close(); }
				} catch (Exception e3) { e3.printStackTrace(); }
			}
		}
	}

	@Override
	public boolean updateTextbook(Textbook textbook) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = getConnection();
			stmt = conn.prepareStatement(__dbProperties.getProperty("sql.updateTextbook"));
			stmt.setString(1, textbook.getTextbookName());
			stmt.setString(2, textbook.getTextbookCode());
			stmt.setString(3, textbook.getTextbookImage());
			stmt.setString(4, textbook.getTextbookARContentDesc());
			stmt.setInt(5, textbook.getTextbookId());
			return (stmt.executeUpdate() > 0);
		} catch (Exception sqe) {
			sqe.printStackTrace();
			return false;
		} finally {  // why nest all of these try/finally blocks?
			try {
					if (stmt != null) { stmt.close(); }
			} catch (Exception e2) { e2.printStackTrace(); }
			finally {
				try {
					if (conn != null) { conn.close(); }
				} catch (Exception e3) { e3.printStackTrace(); }
			}
		}
	}
}