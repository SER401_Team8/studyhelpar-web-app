package edu.asupoly.ser402.studyhelpar.services.impl;

import java.util.List;
import java.util.Set;

import edu.asupoly.ser402.studyhelpar.model.Textbook;

import java.util.ArrayList;
import java.util.LinkedHashSet;

//A simple impl of interface BooktownService
public class SimpleStudyHelparServiceImpl extends AStudyHelparServiceImpl {
	
	// Author section
	private final static String[] names = {"Array", "Python for dummies"};
	private final static String[] codes = {"Database111", "Code222"};
	private final static String[] images = {"image-database", "image-python"};
	private final static String[] arcontents = {"arcontent-database", "arcontent-python"};
	
	private Set<Textbook> __textbooks = null;

	public List<Textbook> getTextbooks() {
		List<Textbook> deepClone = new ArrayList<Textbook>();
		for (Textbook a : __textbooks) {
			deepClone.add(new Textbook(a.getTextbookId(), a.getTextbookName(), a.getTextbookCode(), a.getTextbookImage(), a.getTextbookARContentDesc()));
		}
		return deepClone;
	}

	public int createTextbook(String name, String code, String image, String arcontentdesc) {	
		int textbookId = generateKey(1,99999);
		// 10 retries 
		for (int i = 0; i < 10 && !(__textbooks.add(new Textbook(generateKey(1, 99999), name, code, image, arcontentdesc))); ) {
			textbookId = generateKey(1,99999);
		}
		return textbookId;
    }

	public boolean deleteTextbook(int textbookId) {
		boolean rval = false;
		try {
			/*
			// Find any Books pointing at this textbook
			Set<Book> books = new LinkedHashSet<Book>();
			for (Book b : __books) {
				if (b.getTextbookId() == textbookId) {
					b.setTextbookId(-1);  // I guess -1 will mean marked for deletion
					books.add(b);
				}
			}
			*/

			Textbook a = getTextbook(textbookId);
			if (a != null) {
				rval = __textbooks.remove(a);
			}
			/*
			if (!rval) {
				// if we couldn't remove that book we have to undo the books above,
				// which is why we hung onto them!
				for (Book b : books) {
					b.setTextbookId(authorId);
				}
			}
			*/
		} catch (Exception exc) {
			exc.printStackTrace();
			rval = false;
		}
		return rval;
	}
	
	@Override
	public Textbook getTextbook(int id) {
		for (Textbook a : __textbooks) {
			if (a.getTextbookId() == id) {
				return a;
			}
		}
		return null;
	}

	@Override
	public boolean updateTextbook(Textbook textbook) {
		boolean rval = false;
		for (Textbook a : __textbooks) {
			if (a.getTextbookId() == textbook.getTextbookId()) {
				rval = true;
				a.setTextbookName(textbook.getTextbookName());
				a.setTextbookCode(textbook.getTextbookCode());
				a.setTextbookImage(textbook.getTextbookImage());
				a.setTextbookARContentDesc(textbook.getTextbookARContentDesc());
			}
		}
		return rval;
	}
	
	/*
    // Book section
	private final static String[] titles = {"Sisters First", "My Turn", "Four Days"};
	private Set<Book> __books = null;

    public List<Book> getBooks() {
		List<Book> deepClone = new ArrayList<Book>();
		for (Book b : __books) {
			deepClone.add(new Book(b.getBookId(), b.getTitle(), b.getTextbookId(), b.getSubjectId()));
		}
		return deepClone;
    }
    public Book getBook(int id) {
		for (Book b : __books) {
			if (b.getBookId() == id) {
				return b;
			}
		}
    		return null;	
    }
    
    public int createBook(String title, int aid, int sid) {
		int bookId = generateKey(1,99999);
		// 10 retries 
		for (int i = 0; i < 10 && !(__books.add(new Book(bookId, title, aid, sid))); ) {
			bookId = generateKey(1,99999);
		}
		return bookId;
    }
    
    public Textbook findTextbookOfBook(int bookId) {
    		Author a = null;
    		Book b = getBook(bookId);
    		if (b != null) {
    			a = getAuthor(b.getAuthorId());
    		}
    		return a;
    }
    
    // Subject section
	private final static String[] subjects = {"Humor", "Politics", "Drama"};
	private final static String[] locations = {"Midland, TX", "Little Rock, AR", "Dallas, TX"};
	private Set<Subject> __subjects = null;
	
	public int createSubject(String subject, String location) {
		int subjectId = generateKey(1,99999);
		// 10 retries if we have a key clash
		for (int i = 0; i < 10 && !(__subjects.add(new Subject(subjectId, subject, location))); ) {
			subjectId = generateKey(1,99999);
		}
		return subjectId;
	}
	
    public List<Subject> getSubjects() {
		List<Subject> deepClone = new ArrayList<Subject>();
		for (Subject s : __subjects) {
			deepClone.add(new Subject(s.getSubjectId(), s.getSubject(), s.getLocation()));
		}
		return deepClone;
    }
    public Subject getSubject(int id) {
    		for (Subject s : __subjects) {
    			if (s.getSubjectId() == id) {
    				return s;
    			}
    		}
    		return null;
    }
    public List<Book> findBooksBySubject(int subjectId) {
    		return null;
    }
    
    */
	// Only instantiated by factory?
	public SimpleStudyHelparServiceImpl() {
		__textbooks = new LinkedHashSet<Textbook>();
		//__books = new LinkedHashSet<Book>();
		//__subjects = new LinkedHashSet<Subject>();
		for (int i = 0; i < names.length; i++) {
			int aid = createTextbook(names[i], codes[i], images[i], arcontents[i]);
			//int sid = createSubject(subjects[i], locations[i]);
			//createBook(titles[i], aid, sid);
		}
	}
}
