import React, {Component} from 'react';
import './default.css';
import axios from 'axios';

let id = 0;

function isEven(value) {
    if (value % 2 == 0)
        return true;
    else
        return false;
}

const TableRow = ({name}) => {
    if (isEven(id)) {
        return (
            <tr class="even">
                <td>{id = id + 1}</td>
                <td>{name}</td>
            </tr>
        );
    }
    else {
            return (
                <tr class="odd">
                    <td>{id = id + 1}</td>
                    <td>{name}</td>
                </tr>
            );
        }
}



class ImageList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
        };
        this.handleList = this.handleList.bind(this);
    }

    handleList(e) {
        e.preventDefault();
        axios.get('https://studyhelparnode.herokuapp.com/photos')
            .then(response => {
                this.setState({list: response.data ? response.data : []})
            })
            .catch(err => {
                console.log(err, 'No list, try again');
            });
    }

    render() {
        let $files = null;

        if (this.state.list.length) {
            $files = <div className="tableImage">
                <table className="imageList">
                    <caption>Images</caption>
                    <thead>
                    <tr>
                        <th id="imageID">Nr</th>
                        <th id="imageName">Name</th>
                    </tr>
                    </thead>
                    <tbody>{

                        this.state.list.map(item => {
                                return (
                                    <TableRow
                                        id={id}
                                        name={item.caption}
                                    />
                                );
                            }
                        )
                    }
                    </tbody>
                </table>
            </div>
        }

        return (
            <div>
                <div>
                    <button className="btn" type="submit" onClick={this.handleList}>Get file list from database</button>
                </div>
                <div>
                    {$files}
                </div>
            </div>
        );
    }

}

export default ImageList;

