import React, {Component} from 'react';
import './default.css';
import axios from 'axios';

let id;

class ImageUpload extends Component {

    constructor(props) {
        super(props);
        this.state = {
            file: '',
            name: '',
            comment: '',
            object: '',
            success: '',
            list: []
        };

        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleUpload = this.handleUpload.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.handleList = this.handleList.bind(this);
        this.handlePreview = this.handlePreview.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleUpload(e) {
        e.preventDefault();
        this.setState({
            success: 'File was successfully uploaded to database'
        })

        this.setState({
            file: '',
            comment: '',
            object:'',
            list: []
        })
        document.getElementById("image_uploads").value = null;
    }

    handleRemove(e) {
        e.preventDefault();

        axios.get(`https://studyhelpartest.herokuapp.com/textbooks`, {
            name: document.getElementById("textbook").value,
        })
            .then(response => {
                console.log(name);
                console.log(response, 'Signature added!');
                this.setState({
                    success: 'File was successfully deleted to database'
                })
            })
            .catch(err => {
                console.log(err, 'Signature not added, try again');
                this.setState({
                    success: 'File fail to upload to database'
                })
            });

        this.setState({
            file: '',
            comment: '',
            object: '',
            success: '',
        });

        document.getElementById("image_uploads").value = null;
    }

    handleDelete(e) {
        axios({
            method: 'delete',
            url: 'https://studyhelpartest.herokuapp.com/textbooks/'+ e
        })
        .then(function (response) {
            //handle success
            console.log(response);
        })
        .catch(function (response) {
            //handle error
            console.log(response);
        });

    }

    handlePreview(e) {
        axios.get('https://studyhelpartest.herokuapp.com/textbooks/image/'+ e)
            .then(response => {
                this.setState({image:response.data})
                console.log(response.data)
            })
            .catch(err => {
                console.log(err, 'No list, try again');
            });
    }


    handleImageChange(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        let x = e.target.files[0].name;
        let y = x.split(".");
        let formData = new FormData();
        formData.append("file", file);
        formData.append("name", y[0]);
        formData.append("code", "testcode");
        formData.append("desc", "Test");
        reader.onloadend = () => {
            this.setState({
                file: reader.result,
                comment: x,
                success: ''
            });
        }
        axios({
            method: 'post',
            url: 'https://studyhelpartest.herokuapp.com/textbooks',
            data: formData,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        })
        .then(function (response) {
            //handle success
            console.log(response);
        })
        .catch(function (response) {
            //handle error
            console.log(response);
        });

        reader.readAsDataURL(file)
    }

    handleObjChange(e) {
        e.preventDefault();
        let x = e.target.files[0].name;
        this.setState({
            object: x
        });
    }

    handleList(e) {
        e.preventDefault();
        axios.get('https://studyhelpartest.herokuapp.com/textbooks')

            .then(response => {
                console.log(response.data);
                console.log('post response');
                this.setState({list: response.data ? response.data : []})
            })
            .catch(err => {
                console.log(err, 'No list, try again');
            });
    }


    render() {

        function isEven(value) {
            if (value % 2 == 0)
                return true;
            else
                return false;
        }

        const TableRow = ({name, obj, img}) => {

            if (isEven(id)) {
                return (
                    <tr class="even">
                        <td>{id = id + 1}</td>
                        <td>{name}</td>
                        <td>{obj}</td>
                        <td><button className="btnDelete" type="submit" onClick={(e) => {if (window.confirm("Do you really want to delete " + name +" file from database?")){this.handleDelete(obj); this.forceUpdate();this.setState({
                            success: "Deleting " + name +  " file from database", list: []

                        });} else{
                            this.setState({
                                success: "Cancel or fail to delete " + name +  " file from database",
                            });
                        }}} >Delete</button></td>
                        
                    </tr>
                );
            }
            else {
                return (
                    <tr class="odd">
                        <td>{id = id + 1}</td>
                        <td>{name}</td>
                        <td>{obj}</td>
                        <td><button className="btnDelete" type="submit" onClick={(e) => {if (window.confirm("Do you really want to delete " + name +" file from database?")){this.handleDelete(obj);this.forceUpdate(); this.setState({
                            success: "Deleting " + name +  " file from database", list: []

                        });} else{
                            this.setState({
                                success: "Cancel or fail to delete " + name +  " file from database",
                            });
                        }}} >Delete</button></td>
                        
                    </tr>
                );
            }
        }

        id = 0;
        let $files = null;
        let {comment} = this.state;
        let {success} = this.state;
        let $fileName = null;
        let $uploadButton = null;
        let $removeButton = null;
        let $message = null;

        if (comment) {
            $fileName = <div className="imageName" id="success">Selected file: {comment}</div>
            $uploadButton = <button className="btn" type="submit" onClick={this.handleUpload}>Upload file to database</button>
            $removeButton = <button type="button" className='btn' onClick={this.handleRemove}>Remove selected file</button>;
        }

        if(this.state.imageObj){
            var binaryData = this.state.imageObj;
            $objPreview =
                <div>
                    <img src="https://studyhelpartest.herokuapp.com/textbooks/image/5c98be17e16651000420c106"/>
                    
                </div>
        }

        if (success){
            $message = <div className="imageName" id="success">{success}</div>
        }

        if (this.state.list.length) {
            $files = <div className="tableImage">
                <table className="imageList">
                    <caption>Images</caption>
                    <thead>
                    <tr>
                        <th id="imageID">Nr</th>
                        <th id="imageName">Name</th>
                        <th id="objName">Object</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>{

                        this.state.list.map(item => {
                            console.log(item._id)
                                return (
                                    <TableRow
                                        id={id}
                                        name={item.name}
                                        obj={item._id}
                                        img={item.imageId}

                                    />
                                );
                            }
                        )
                    }
                    </tbody>
                </table>
            </div>
        }

        return (
            <div>
                <form onSubmit={this.handleUpload}>
                <div id="portfolio">
                    <h3>Please select an image and corresponding 3D model</h3></div>
                    <label for="image_uploads" className="btn">Choose a file</label>
                    <input id="image_uploads" className="upload-btn-wrapper" type="file"
                           onChange={this.handleImageChange} />
                    <label for="obj_uploads" className="btn">Choose an object</label>
                    <input id="obj_uploads" className="upload-btn-wrapper" type="file"/>
                    <div>
                        {$fileName}
                        {$removeButton}
                        {$uploadButton}
                        <img src = {this.state.image}></img>
                    </div>
                    <div>
                        {$message}
                    </div>
                    <div>
                        <button className="btn" type="submit" onClick={this.handleList}>Get file list from database</button>
                    </div>
                    <div>
                        {$files}
                    </div>
                </form>
            </div>
        )
    }

}

export default ImageUpload;