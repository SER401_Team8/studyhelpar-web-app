import React, {Component} from 'react';
import './default.css';
import axios from 'axios';
import ObjImage from "./ObjImage";
import Select from 'react-select';

type State = {
    options: [{ [string]: string }],
    value: string | void,
}

const createOption = (label: string) => ({
    label,
    value: label.toLowerCase().replace(/\W/g, ''),
})


class ObjUpload extends Component <*, State>{

    constructor(props) {
        super(props);
        this.state = {
            file: '',
            textbook: '',
            comment: '',
            success: '',
            bookSelected: null,
            textbookList: [],
            sel: null,
            bList: [],
        };

        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleUpload = this.handleUpload.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.handlePreview = this.handlePreview.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleBookChange = this.handleBookChange.bind(this);
        this.handleTextbook = this.handleTextbook.bind(this);
    }

    handleBookChange(bookSelected) {
        this.setState({bookSelected: bookSelected, sel: bookSelected.value});
    }
    
    handleUpload(e) {
        e.preventDefault();
        console.log(this.state.file)
        //Split sting in half
        let length = this.state.file.length
        let first_length = Math.round(length/3)
        let first_half = this.state.file.slice(0, first_length)
        let second_half = this.state.file.slice(first_length, (first_length * 2))
        let third_half = this.state.file.slice((first_length*2) , length)
        //post first half, the second
        //have second post and join the strings
        console.log(first_half)
        console.log(second_half)
        console.log(third_half)
        axios.post('https://studyhelparnode.herokuapp.com/objupload', {
            file: first_half,
            comment: this.state.comment,
            textbook: document.getElementById("textbook").value,
            second: false,
        })
            .then(response => {
                console.log(response, 'Signature added!');
                console.log(response, this.state.file);
                console.log(response, this.state.comment);
                this.setState({
                    success: 'File was successfully uploaded to database'
                })
            })
            .catch(err => {
                console.log(err, 'Signature not added, try again');
                console.log(response, this.state.file);
                console.log(response, this.state.comment);
                this.setState({
                    success: 'File fail to upload to database'
                })
            });

            axios.post('https://studyhelparnode.herokuapp.com/objupload', {
            file: second_half,
            comment: this.state.comment,
            textbook: document.getElementById("textbook").value,
            second: true,
        })
            .then(response => {
                console.log(response, 'Signature added!');
                console.log(response, this.state.file);
                console.log(response, this.state.comment);
                this.setState({
                    success: 'File was successfully uploaded to database'
                })
            })
            .catch(err => {
                console.log(err, 'Signature not added, try again');
                console.log(response, this.state.file);
                console.log(response, this.state.comment);
                this.setState({
                    success: 'File fail to upload to database'
                })
            });

            axios.post('https://studyhelparnode.herokuapp.com/objupload', {
                file: third_half,
                comment: this.state.comment,
                textbook: document.getElementById("textbook").value,
                second: true,
            })
                .then(response => {
                    console.log(response, 'Signature added!');
                    console.log(response, this.state.file);
                    console.log(response, this.state.comment);
                    this.setState({
                        success: 'File was successfully uploaded to database'
                    })
                })
                .catch(err => {
                    console.log(err, 'Signature not added, try again');
                    console.log(response, this.state.file);
                    console.log(response, this.state.comment);
                    this.setState({
                        success: 'File fail to upload to database'
                    })
                });
    

        this.setState({
            file: '',
            comment: ''
        })
        document.getElementById("image_uploads").value = null;

    }

    handleRemove(e) {
        e.preventDefault();

        this.setState({
            file: '',
            comment: '',
            success: ''
        });

        document.getElementById("image_uploads").value = null;
    }

    handleDelete(e) {
        e.preventDefault();
    }

    handlePreview(e) {
        e.preventDefault();
        this.setState({
            success: 'Looking at Obj file',
        });
    }

    handleImageChange(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        let x = e.target.files[0].name;
        reader.onloadend = () => {
            this.setState({
                file: reader.result,
                comment: x,
                success: ''
            });
        }

        reader.readAsText(file);
    }

    handleTextbook() {
        axios.get('https://studyhelpar.herokuapp.com/textbooks')
            .then(response => {
                this.setState({textbookList : response.data.map(item => item.name), bList: response.data ? response.data : []})
                console.log(textbookList);
            })
            .catch(err => {
                console.log(err, 'No list, try again');
            });
    }

    componentDidMount() {
        this.handleTextbook();
    }



    render() {

        let {comment} = this.state;
        let {success} = this.state;
        let {file} = this.state;
        let $fileName = null;
        let $uploadButton = null;
        let $removeButton = null;
        let $message = null;
        let $objPreview = null;
        let $listPreview = null;
        const {bookSelected, sel} = this.state;

        let arrayTextbook = this.state.textbookList;
        let books = [];
        let b = null;


        let books2 = arrayTextbook.forEach(function(obj) {
            if (books.indexOf(obj) === -1) books.push(obj);
        });


        const bookOptions = books.map(v => (
            createOption(v)
        ));

        let bookList = this.state.bList
            .filter((item) => item.name == sel)
            .map((item) => item.desc);



        if(file){
            $objPreview =
                <div>
                    <ObjImage obj={this.state.file}/>
                </div>
        }

        if(sel){

            $listPreview = <h3>{sel} has these images: {bookList}</h3>
        }

        if (comment) {
            $fileName = <div className="imageName" id="success">Selected file: {comment}</div>
            $uploadButton = <button className="btn" type="submit" onClick={this.handleUpload}>Upload file to database</button>
            $removeButton = <button type="button" className='btn' onClick={this.handleRemove}>Remove selected file</button>;
        }

        if (success){
            $message = <div className="imageName" id="success">{success}</div>
        }


        return (
            <div>
                <form onSubmit={() => {this.handleUpload()}}>
                <div id="portfolio">
                    <h3>Select a textbook from the database </h3>
                    <Select className="selectBook"
                            onChange={this.handleBookChange}
                            placeholder="Select a textbook"
                            options={bookOptions}
                            isClearable
                            placeholder="Select textbook"
                            isSearchable
                            value={bookSelected}
                            closeMenuOnSelect={false}
                    />

                    <h3>Or enter new textbook name below</h3>
                    <input type="text" id="textbook" value={sel}></input>
                    </div>
                    <label for="image_uploads" className="btn">Choose a file</label>
                    <input id="image_uploads" className="upload-btn-wrapper" type="file"
                           onChange={this.handleImageChange} />
                    <div>
                       {$objPreview}
                        {$fileName}
                        {$removeButton}
                        {$uploadButton}
                    </div>
                    <div>
                        {$message}
                    </div>
                    <div id="portfolio">
                        <h3>Selected textbook is: {sel}</h3>
                        <div> {$listPreview}</div>
                    </div>
                </form>
            </div>
        )
    }

}

export default ObjUpload;