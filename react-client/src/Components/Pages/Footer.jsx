import React, {Component} from 'react';
import './default.css';

class Footer extends Component {
    render() {
        return (
            <div id="copyright" className="container">
                <p>© Untitled. All rights reserved. | Photos by <a href="http://fotogrph.com/">Fotogrph</a> | Design
                    by <a href="http://templated.co" rel="nofollow">TEMPLATED</a>.</p>
            </div>
        );
    }
}

export default Footer;