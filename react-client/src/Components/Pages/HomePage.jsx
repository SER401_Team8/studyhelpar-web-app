import React, {Component} from 'react';
import './default.css';

class HomePage extends Component {
    render() {
        return (

            <div id="wrapper2">
                <div id="welcome" className="container">
                    <div className="title">
                        <h2>Welcome to StudyHelpAR</h2>
                    </div>
                    <p>This is <strong>StudyHelpAR</strong>, an educational augmented reality application. This web app
                        serves as the backend to the Android application. The goal is to supplement educational
                        textbooks and eBooks with augmented reality content using the user’s Android mobile phone. The
                        application reads an image or code printed in the book and renders the associated augmented
                        reality content provided by the publisher. Any student with access to an Android device in their
                        household or classroom will then be presented with new ways to view and interact with the
                        content to help increase their understanding of the material. </p>
                </div>
            </div>
        );
    }
}

export default HomePage;
