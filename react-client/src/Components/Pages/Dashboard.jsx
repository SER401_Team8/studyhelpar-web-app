import React from 'react';
import Images from './Image.jsx';
import HomePage from './HomePage.jsx';
import About from './About.jsx';
import Header from './Header.jsx';
import Footer from './Footer.jsx';
import {Route} from 'react-router-dom';
import './default.css'

const Dashboard = () => (
    <div id="dashboard">
        <Header/>
        <div className="content">
            <Route exact path="/" component={HomePage}/>
            <Route exact path="/images" component={Images}/>
            <Route exact path="/about" component={About}/>
        </div>
        <Footer/>
    </div>
)

export default Dashboard;
