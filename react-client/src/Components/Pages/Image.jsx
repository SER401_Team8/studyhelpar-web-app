import React from 'react';
import ImageUpload from "./ImageUpload";

// The Roster component matches one of two different routes
// depending on the full pathname
const Image = () => (
    <div id="wrapper2">
        <div>
            <ImageUpload />
        </div>
    </div>
)

export default Image;