
import React, {Component} from 'react';
import './default.css';
import { oneOfType, string, number } from 'prop-types';

const strNyum = oneOfType([string, number]);

class About extends Component {
    
      
    
    
 
    render() {
       

        return (
            <div id="wrapper2">
        <div id="wrapper2">
                <div id="welcome" className="container">
                    <div className="title">
                        <h2>Team 8 </h2>
                    </div>
                    <p>Thank you for using our web application. We are 
                        Team 8 from SER 402 at Arizona State University. 
                        This is our serior capstone project. Our team
                        consist of 5 members.
                        <br/><strong>* Sara</strong>
                        <br/><strong>* Gerda</strong>
                        <br/><strong>* Teresa</strong>
                        <br/><strong>* Anup</strong>
                        <br/><strong>* Andrew</strong>
                        <br/>Our team created this web application
                        to go along with our augmented reality 
                        Android application, Educational Augmented
                        Reality. This is all part of our Senior capstone
                        project. You can see the Android application in 
                        action <a href="https://youtu.be/himoI3dS54U">HERE.</a>
                        <br/><a href="https://tree.taiga.io/project/sachten-group-8/timeline">Team's Taiga</a>
                        <br/><a href="https://gitlab.com/SER401_Team8?nav_source=navbar">Team's Repository</a>
                    </p>
                </div>
            </div>
            <form action="https://postmail.invotes.com/send"
              method="post" id="email_form">

              <input type="text" name="subject" placeholder="Subject" />
              <textarea name="text" placeholder="Message"></textarea>
              <input type="hidden" name="access_token" value="wo3dkwcmlqsrgie18k086n7j" />
                
              <input type="hidden" name="success_url" value=".?message=Email+Successfully+Sent%21&isError=0" />
              <input type="hidden" name="error_url" value=".?message=Email+could+not+be+sent.&isError=1" />
            
              <input id="submit_form" type="submit" value="Send" />
              <p>Powered by <a href="https://postmail.invotes.com" target="_blank">PostMail</a></p>
          </form>
    </div>

        )
    }
}

export default About;