import React, {Component} from 'react';

import {NavLink} from 'react-router-dom';
import './default.css'

class Header extends Component {
    render() {
        return (
            <div id="wrapper1">
                <div id="header-wrapper">
                    <div id="header" class="container">
                        <div id="logo"><span><img src={require('./images/logo.png')}/></span>
                            <h1><a href="#">StudyHelpAR</a></h1>
                            <span>Project by <a href="http://templated.co" rel="nofollow">Team 8</a></span></div>
                        <div id="menu">
                            <ul>
                                <li><NavLink exact to="/" activeStyle={{border: '1px solid #3C3C3C'}}>
                                    Home
                                </NavLink></li>
                                <li><NavLink exact to="/images" activeStyle={{border: '1px solid #3C3C3C'}}>
                                    Images
                                </NavLink></li>
                                <li><NavLink exact to="/about" activeStyle={{border: '1px solid #3C3C3C'}}>
                                    About
                                </NavLink></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;