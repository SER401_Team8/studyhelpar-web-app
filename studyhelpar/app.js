const Express = require("express");
const BodyParser = require("body-parser");
const Mongo = require('mongodb');
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;
const Grid = require('gridfs-stream');
var multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const cors = require('cors');

//const CONNECTION_URL = "mongodb+srv://team8admin:team8ser402@cluster0-ffvo3.mongodb.net/admin?retryWrites=true";
const CONNECTION_URL = "mongodb+srv://team8admin:team8ser402@cluster0-ffvo3.mongodb.net/studyhelpar?retryWrites=true";
const DATABASE_NAME = "studyhelpar";

var app = Express();

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(cors());

var database, collection, imageCollection;
var gfs,storage;
var upload, sUpload;
var connError = null;
var connErrorMess = "Failed to connect to the database";

require('dotenv').config();

//const client = MongoClient.connect(CONNECTION_URL, { useNewUrlParser: true });
MongoClient.connect(CONNECTION_URL, { useNewUrlParser: true }, (error, client) => {
    if(error) {
        connError = connErrorMess;
    }
    try{
        database = client.db(DATABASE_NAME);
        collection = database.collection("textbook");
        imageCollection = database.collection("fs");
        console.log("Connected to database: `" + DATABASE_NAME + "`!");

        // Init stream
        gfs = Grid(database,Mongo); 
        gfs.collection('fs');

    }
    catch{
        connError = connErrorMess;
    }
});

storage = new GridFsStorage({ 
    url: CONNECTION_URL,
    file: (req, file) => {
        console.log("in GridFsStorage");
        return new Promise((resolve, reject) => {
            const filename = file.originalname;
            const fileInfo = {
              filename: filename,
              bucketName: 'fs'
            };
            resolve(fileInfo);
        });
    }
});
upload = multer({ //multer settings for single upload
    storage: storage
});

sUpload = upload.single('file');


app.listen(process.env.PORT || 3000, () => {
	console.log("Connected to port = " + process.env.port);
});

app.get("/textbooks", (request, response) => {
    if(connError != null){
        return response.status(500).send(connErrorMess);
    }

    try{
        collection.find({}).toArray((error, result) => {
            if(error) {
                return response.status(500).send(error);
            }
            response.send(result);
        });
    }
    catch{
        connError = connErrorMess;
    }
});

app.get("/textbooks/:id", (request, response) => {
    if(connError != null){
        return response.status(500).send(connErrorMess);
    }

    try{
        collection.findOne({ "_id": new ObjectId(request.params.id) }, (error, result) => {
            if(error) {
                return response.status(500).send(error);
            }
            response.send(result);
        });
    }
    catch{
        connError = connErrorMess;
    }
});

app.get("/textbooks/image/:id", (request, response) => {
    if(connError != null){
        return response.status(500).send(connErrorMess);
    }

    try{
        var readstream = gfs.createReadStream({
        	_id: request.params.id
        });
        readstream.pipe(response);
    }
    catch (error){
        console.log(error);
        response.status(500).send(error);
    }
});

/** API path that will upload the files */
app.post('/textbooks', sUpload, (request, response) => {
    if(connError != null){
        return response.status(500).send(connErrorMess);
    }

    console.log("During upload");
    
    var file = request.body.file;
    if(!file){
        const error1 = new Error('Please upload a file');
        return response.status(500).send(error1);
    }

    var myobj = {name: request.body.name, code: request.body.code, desc: request.body.desc, imageId: file.id};

    collection.insertOne(myobj, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
    });

    console.log(request.body.name);
    response.status(200).send("Upload successful");
});


/*
app.put("/textbooks", async (request, response) => {
    try {
        var textbook = await collection.findById(request.params.id).exec();
        textbook.set(request.body);
        var result = await textbook.save();
    } catch (error) {
        response.status(500).send(error);
    }

    try {
        var image = await gfs.findById(request.params.id).exec();
        image.set(request.body);
        var result = await image.save();
    } catch (error) {
        response.status(500).send(error);
    }

    response.send(result);
});
*/

app.delete("/textbooks/:id", async (request, response) => {
    if(connError != null){
        return response.status(500).send(connErrorMess);
    }

    try {
        var imageId;

        collection.findOne({ "_id": new ObjectId(request.params.id) }, (error, result) => {
            if(error) {
                return response.status(500).send(error);
            }
            imageId = result.imageId;
            console.log("MyObject: " + imageId);

            collection.deleteOne({ "_id": new ObjectId(request.params.id) }, (error2, result2) => {
                if(error2) {
                    return response.status(500).send(error);
                }

                gfs.remove({ _id: imageId });
            });

            response.status(200).send("Delete successful");
        });
    } catch (error) {
        console.log(error);
        response.status(500).send(error);
    }
});