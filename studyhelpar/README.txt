Download node.js

Installing the following via npm:
	npm install express-generator -g
	npm install -g nodemon --save
	npm install -g mongodb --save
	npm install -g gridfs-stream --save
	npm install -g dotenv --save
	npm install multer-gridfs-storage --save
	
Run the app:
	node app.js
	
Deploying on heroku
	Go to your root directory of your app
	heroku login 						//Login to heroku
	git init  							// initialize git repo
	heroku create --app studyhelpar  	// create heroku app
	git add . 							// add to git
	git commit -m "creating app" 		// commit to git
	git push heroku master 				// push it to heroku
	your app is deployed!!!
	
Test your app by going to the browser
	(GET) https://studyhelpar.heroku.com/textbooks
	(GET) https://studyhelpar.heroku.com/textbooks/[textbookId]
	(GET) https://studyhelpar.heroku.com/textbooks/image/[imageId]
	(POST) https://studyhelpar.heroku.com/textbooks
	(DELETE) https://studyhelpar.heroku.com/textbooks/[textbookId]
	
